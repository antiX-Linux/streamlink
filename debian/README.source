# Build procedure

Here are the commands to manage the package:
- Import upstream version and update Debian branch with new upstream release:
  - `gbp import-orig --uscan`

- Refresh quilt patches:
  - `quilt pop -a && while quilt push; do quilt refresh; done`

- Check for important changes that may need other package changes (copyright, new patch, ...):
  - `https://github.com/streamlink/streamlink/compare/<old_tag>...<new_tag>`

- Build and test the package:
  - `debuild`

- Release version with `low` urgency:
  - `gbp dch -Ra --commit -U low`

- Build source package and tag Debian branch:
  - `gbp buildpackage --git-tag --git-sign-tags -nc -S`

- Build package using sbuild:
  - `sbuild -As ../streamlink_<version>.dsc -d unstable`
  - `gbp buildpackage "--git-builder=sbuild -v -As -d unstable --run-lintian --lintian-opts=\"-EviIL +pedantic\" --run-autopkgtest --autopkgtest-root-args= --autopkgtest-opts=\"-- schroot %r-%a-sbuild\""`

- Run lintian:
  - `lintian -EviIL +pedantic  ./streamlink_<version>_amd64.changes`

- Don't forget to push `master` and `upstream` branches along with new tags (`debian/<version>`, `upstream/<version>`)


# Packages structure

## streamlink

This package contains the command line tool which use the public Python module available in python3-streamlink package.

This package contains the Python module `streamlink_cli` which is private and so goes to /usr/share/streamlink.
The command line script is also in /usr/share/streamlink so it can find the Python module.
/usr/bin/streamlink is a symlink to the command line script.

See also an advice about why not putting the command line script in /usr/bin:
  https://lists.debian.org/debian-python/2016/05/msg00046.html

As recommended in dh_installman manpage, manpage installation is defined in streamlink.manpages.

## python3-streamlink

This package contains the public Python module `streamlink`.

## python3-streamlink-doc

This package contains streamlink documentation in html format.
